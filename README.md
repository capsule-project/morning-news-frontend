# Projet Frontend

## Introduction

Ceci est la partie frontend du projet MorningNews, construite avec Next.js. Ce guide vous aidera à configurer et exécuter le projet en utilisant Docker.

## Prérequis

- Docker installé sur votre machine
- Yarn package manager installé sur votre machine (pour le développement local)

## Développement Local

### Installer les Dépendances
- Avant de lancer le projet localement, vous devez installer les dépendances :

	
```sh
yarn install
```

## Démarrer le serveur de développement

```sh
yarn dev
```
	
L’application sera accessible à http://localhost:3000.

## Dockerfile

### Construire l’Image Docker

Pour construire l’image Docker du projet frontend, naviguez jusqu’au répertoire frontend et exécutez la commande suivante :
```sh
docker build -t morning-news-frontend .
```

## Exécuter le Conteneur Docker

Après avoir construit l’image Docker, vous pouvez exécuter un conteneur en utilisant la commande suivante :

```sh
docker run -p 3000:3000 morning-news-frontend 
```

L’application sera accessible à http://localhost:3000.