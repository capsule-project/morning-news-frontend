# Utiliser une image Node.js comme étape de build
FROM node:14 as build

# Définir le répertoire de travail
WORKDIR /app

# Copier les fichiers package.json et yarn.lock
COPY package.json yarn.lock ./

# Installer les dépendances
RUN yarn install

# Copier le reste des fichiers du projet
COPY . .

# Construire l'application pour la préproduction
RUN yarn build

# Utiliser une image Node.js pour servir l'application Next.js
FROM node:14 as run

# Définir le répertoire de travail
WORKDIR /app

# Copier les fichiers de build de l'étape précédente
COPY --from=build /app ./

# Installer les dépendances de production uniquement
RUN yarn install --frozen-lockfile --production

# Exposer le port 3000
EXPOSE 3001

# Démarrer l'application Next.js
CMD ["yarn", "start"]